package com.tcs.tcsfullprojectdemo;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class TcsFullprojectDemoApplicationTests {

	@Test
	void contextLoads() {
	}

	@Test
	public void tc1() {
		int a = 5;
		assertTrue(a<=10, "The value of 'a' is greater than 10");
	}
	
	@Test
	public void tc2() {
		String name = "dan";
		assertTrue(name.equals("dan"));
	}
	
	@Test
	public void tc3() {
		int a = 5;
		assertTrue(a>=5 && a<=10);
	}

	@Test
	public void tc4() {
		int a = 5;
		assertTrue(a>=5 && a<=10);
	}
	
	@Test
	public void tc5() {
		int a = 5;
		assertTrue(a>=5 && a<=10);
	}
	
	@Test
	public void tc6() {
		int a = 5;
		assertTrue(a>=5 && a<=10);
	}
}
